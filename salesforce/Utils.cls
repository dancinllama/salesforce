/**
 * Salesforce utility methods
 * @author CodeFriar and Dancing Llama, Hidden Alpaca
 * @date Sometime in 2013
 */
public class Utils {

    // refactored from: http://stackoverflow.com/questions/9369653/retrieve-salesforce-instance-url-instead-of-visualforce-instance
    // Returns the Salesforce Instance that is currently being run on,
    // e.g. na12, cs5, etc.
    public static String Instance {
        public get {
            //Salesforce, why you no have switch case?!?
            if (Instance == null) {
            // Split up the hostname using the period as a delimiter there are 3 options:
                List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
            // (2) na12.salesforce.com      --- 3 parts, Instance is 1st part
                if (parts.size() == 3) Instance = parts[0];
            // (1) my--test1--nexus.cs0.visual.force.com  --- 5 parts, Instance is 2nd part
                else if (parts.size() == 5) Instance = parts[1];
            // (3) my.domain.salesforce.com    --- 4 parts, Instance is not determinable
                else Instance = null;
            } return Instance;
        } private set;
    }

    /**
     * getBaseURLForInstance
     * @description Gets the SFDC URL pertaining to the current instance the code is running on.
     * @return a String representing the HTTPS URL of the current Salesforce domain
     */
    public static String getBaseUrlForInstance(){
        return 'https://' + Instance + '.salesforce.com';
    }

    /**
     * getManagedPackagePrefix
     * @description Gets the current prefix associated with the code.
     * If the prefix is non existent (aka this is on a dev org with no associated prefix, then empty
     * string is returned.  Otherwise, prefix + '__' is returned.
     * @return a String pertaining to the current namespace.
     */
    public static String getManagedPackagePrefix(){
        //Replace Utils in the query below with whatever class you're saving this method into.
        String prefix = [Select NamespacePrefix From ApexClass Where Name='Utils'].NamespacePrefix;
        //Returning empty string because it is used elsewhere for concatenating the prefix into queries.
        //Empty string may be replaced with null if you prefer to null-check elsewhere...
        return (prefix == null) ? '' : prefix+'__';
    }
}
