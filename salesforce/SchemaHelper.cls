public with sharing class SchemaHelper {


 	public static Schema.SObjectType getObjectTypeFromId(Id objectId){
 		
 		if (objectId == null) {
 			throw new SchemaHelperException('The objectId parameter is null.');
 		}
 		
		Schema.SObjectType result;
		string target = objectId;
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		string keyPrefix;
		for(Schema.SObjectType describe: gd.values() ){
			keyPrefix = describe.getDescribe().getKeyPrefix();
			if(keyPrefix != null && target.startsWith(keyPrefix)){
				result = describe;
				break; //no need to keep looking
			}
		}
		return result;
	}
	
	public static String getKeyPrefixFromObjectName(String objectName) {
		
		if (objectName == null) {
 			throw new SchemaHelperException('The objectName parameter is null.');
 		}
		
		String result;
		string target = objectName;
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		string objectApiName;
		for (Schema.SObjectType describe : gd.values()) {
			objectApiName = describe.getDescribe().getName();
			if (objectApiName == objectName) {
				result = describe.getDescribe().getKeyPrefix();
				break;
			}
		}
		return result;
	}
	
	public class SchemaHelperException extends Exception {}
	
}