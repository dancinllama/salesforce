/**
 * InstallScriptExampleTest
 * @description Test class for InstallScriptExample
 * @author Dancing Llama, Hidden Alpaca
 * @date 5/6/2013
 */
@isTest
private class InstallScriptExampleTest{

    /**
     * testOnInstall
     * @description test clean install of package
     */
    static testMethod void testOnInstall(){
        InstallScriptExample is = new InstallScriptExample();
        Test.testInstall(is,null);

        My_Custom_Hierarchy_Setting__c s1 = My_Custom_Hierarchy_Setting__c.getOrgDefaults();
        //Assert s1 and its fields are not null

        Map<String,My_Custom_List_Setting__c> s2 = My_Custom_List_Setting__c.getAll();
        System.assertNotEquals(null,s2);
        System.assertNotEquals(0,s2.values().size());
        //Assert fields are populated...

        Record_Type_Id_Setting__c s3 = Record_Type_Id_Setting__c.getOrgDefaults();
        //Assert s3 and its fields are not null
    }

    /**
     * testUpgrade
     * @description test package upgrade
     */
    static testMethod void testUpgrade(){
        InstallScriptExample is = new InstallScriptExample();
        //Test initial install
        Test.testInstall(is,null);
        //Test upgrade
        Test.testInstall(is,new Version(1,1));

        //System asserts go here
    }
}
